#import <Foundation/Foundation.h>

#define ID @"id"
#define FIRST_NAME @"firstName"
#define LAST_NAME @"lastName"
#define IDENTITY_CODE @"identityCode"
#define NOTE @"note"
#define BIRTH_DATE @"birthDate"

@interface Customers : NSObject

+ (void)loadCustomersWithBlock:(void (^)(NSArray *addresses, NSError *error))block;
+ (void)createCustomer:(NSDictionary *)customer withBlock:(void (^)(NSDictionary *customer, NSError *error))block;
+ (void)updateCustomer:(NSDictionary *)customer withBlock:(void (^)(NSDictionary *customer, NSError *error))block;
+ (void)deleteCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSError *error))block;;

@end
