#import "NSDate+CustomerBirthDate.h"


@implementation NSDate (CustomerBirthDate)

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    dateFormatter.timeStyle = NSDateFormatterNoStyle;
    return dateFormatter;
}

+ (NSDateFormatter *)iso8601DateFormatter {
    NSDateFormatter *isoDateFormatter = [[NSDateFormatter alloc] init];
    isoDateFormatter.dateFormat = @"yyyy-MM-dd";
    return isoDateFormatter;
}

+ (NSString *)formattedDateFromISO8601Formatted:(NSString *)iso8601Formatted {
    if (iso8601Formatted == [NSNull null]) {
        return nil;
    }

    NSDate *date = [[self iso8601DateFormatter] dateFromString:iso8601Formatted] ;
    return [[self dateFormatter] stringFromDate:date];
}

+ (NSString *)iso8601FormattedFromFormattedDate:(NSString *)formattedDate {
     NSDate *date = [self.dateFormatter dateFromString:formattedDate];
     return [[self iso8601DateFormatter] stringFromDate:date] ?: [NSNull null];
}

- (NSString *)formattedDate {
    return [[NSDate dateFormatter] stringFromDate:self];
}

@end