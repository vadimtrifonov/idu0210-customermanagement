#import "CustomerTableViewController.h"
#import "Customers.h"
#import "AddressesTabelViewController.h"
#import "SVProgressHUD.h"
#import "NSDate+CustomerBirthDate.h"

@interface CustomerTableViewController () <UITextFieldDelegate>
@property(weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property(weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property(weak, nonatomic) IBOutlet UITextField *identityCodeTextField;
@property(weak, nonatomic) IBOutlet UITextField *noteTextField;
@property(weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property(strong, nonatomic) IBOutlet UIDatePicker *birthdayDatePicker;
@end

@implementation CustomerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.birthdayTextField.inputView = self.birthdayDatePicker;

    self.firstNameTextField.text = self.customer[FIRST_NAME];
    self.lastNameTextField.text = self.customer[LAST_NAME];
    self.identityCodeTextField.text = self.customer[IDENTITY_CODE];
    self.noteTextField.text = self.customer[NOTE];
    self.birthdayTextField.text = [NSDate formattedDateFromISO8601Formatted:self.customer[BIRTH_DATE]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showAddresses"] || [segue.identifier isEqualToString:@"showContacts"])  {
        [segue.destinationViewController setCustomerId:self.customer[ID]];
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    if (editing) {
        return;
    }

    [self updateCustomer];
    [self.view endEditing:YES];
}

- (void)updateCustomer {
    NSMutableDictionary *updatedCustomer = [self.customer mutableCopy];
    updatedCustomer[FIRST_NAME] = self.firstNameTextField.text;
    updatedCustomer[LAST_NAME] = self.lastNameTextField.text;
    updatedCustomer[IDENTITY_CODE] = self.identityCodeTextField.text;
    updatedCustomer[NOTE] = self.noteTextField.text;
    updatedCustomer[BIRTH_DATE] = [NSDate iso8601FormattedFromFormattedDate:self.birthdayTextField.text];

    if (![self.customer isEqualToDictionary:updatedCustomer]) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [Customers updateCustomer:updatedCustomer withBlock:^(NSDictionary *customer, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:@"Failed"];
            }
            else {
                self.customer = customer;
                [self.delegate customerTableViewControllerDidUpdateCustomer:customer];
                [SVProgressHUD showSuccessWithStatus:@"Updated"];
            }
        }];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Actions

- (IBAction)birthdayDateChanged {
    self.birthdayTextField.text = self.birthdayDatePicker.date.formattedDate;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return self.tableView.editing;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITableViewDataSource

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

@end
