@interface NSDate (CustomerBirthDate)

+ (NSString *)iso8601FormattedFromFormattedDate:(NSString *)formattedDate;
+ (NSString *)formattedDateFromISO8601Formatted:(NSString *)iso8601Formatted;

- (NSString *)formattedDate;

@end