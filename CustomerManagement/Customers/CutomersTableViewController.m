#import "CutomersTableViewController.h"
#import "Customers.h"
#import "CustomerTableViewController.h"
#import "NewCustomerTableViewController.h"
#import "SVProgressHUD.h"

@interface CutomersTableViewController () <UISearchDisplayDelegate, UISearchBarDelegate, CustomerTableViewControllerDelegate, NewCustomerTableViewControllerDelegate>
@property (strong, nonatomic) NSMutableArray *customers;
@property (strong, nonatomic) NSMutableArray *searchResults;
@end

@implementation CutomersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];    
    [self.refreshControl addTarget:self
                            action:@selector(refresh:)
                  forControlEvents:UIControlEventValueChanged];
    
    self.searchResults = [NSMutableArray array];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self loadCustomers];
}

- (void)loadCustomers {
    [Customers loadCustomersWithBlock:^(NSArray *customers, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:@"Failed"];
        }
        else {
            self.customers = [customers mutableCopy];
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        }

        [self.refreshControl endRefreshing];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCustomer"]) {
        NSIndexPath *indexPath = [(self.searchDisplayController.active ? self.searchDisplayController.searchResultsTableView : self.tableView) indexPathForSelectedRow];
        NSDictionary *customer = (self.searchDisplayController.active ? self.searchResults : self.customers)[indexPath.row];
        
        [segue.destinationViewController setDelegate:self];
        [segue.destinationViewController setCustomer:customer];
    }
    else if ([segue.identifier isEqualToString:@"newCustomer"]) {
        UINavigationController *nc = segue.destinationViewController;
        [(CustomerTableViewController *) nc.topViewController setDelegate:self];
    }
}

#pragma mark - Actions

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self loadCustomers];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (tableView == self.searchDisplayController.searchResultsTableView) ? self.searchResults.count : self.customers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Customer Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *customer = (tableView == self.searchDisplayController.searchResultsTableView) ? self.searchResults[indexPath.row] : self.customers[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", customer[FIRST_NAME], customer[LAST_NAME]];
    cell.detailTextLabel.text = customer[IDENTITY_CODE];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableView != self.searchDisplayController.searchResultsTableView;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [Customers deleteCustomerWithId:self.customers[indexPath.row][ID] withBlock:^(NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:@"Failed"];
            }
            else {
                [self.customers removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [SVProgressHUD showSuccessWithStatus:@"Deleted"];
            }
        }];
    }
}

#pragma mark - UISearchDisplayDelegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self updateSearchResultsForSearchString:searchString];
    return YES;
}

- (void)updateSearchResultsForSearchString:(NSString *)searchString {
    [self.searchResults removeAllObjects];

    searchString = [searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (!searchString.length) {
        return;
    }

    NSArray *searchItems = [searchString componentsSeparatedByString:@" "];
    NSMutableArray *matchPredicates = [NSMutableArray array];

    for (NSString *searchItem in searchItems) {
        NSExpression *leftExpression = [NSExpression expressionForAggregate:@[[NSExpression expressionForKeyPath:FIRST_NAME], [NSExpression expressionForKeyPath:LAST_NAME], [NSExpression expressionForKeyPath:IDENTITY_CODE]]];
        NSExpression *rightExpression = [NSExpression expressionForConstantValue:searchItem];
        [matchPredicates addObject:[NSComparisonPredicate predicateWithLeftExpression:leftExpression
                                                                      rightExpression:rightExpression
                                                                             modifier:NSAnyPredicateModifier
                                                                                 type:NSContainsPredicateOperatorType
                                                                              options:NSCaseInsensitivePredicateOption]];
    }

    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:matchPredicates];
    self.searchResults = [[self.customers filteredArrayUsingPredicate:compoundPredicate] mutableCopy];
}

#pragma mark - NewCustomerTableViewControllerDelegate

- (void)newCustomerTableViewControllerDidCancel {
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)newCustomerTableViewControllerDidGoBackWithNewCustomer:(NSDictionary *)customer {
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [Customers createCustomer:customer withBlock:^(NSDictionary *customer,  NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:@"Failed"];
        }
        else {
            [self.customers addObject:customer];
            [self.tableView reloadData];
            [SVProgressHUD showSuccessWithStatus:@"Created"];
        }
    }];
}

#pragma mark - CustomerTableViewControllerDelegate

- (void)customerTableViewControllerDidUpdateCustomer:(NSDictionary *)customer {
    __block NSUInteger customerIdx = NSNotFound;
    [self.customers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj[ID] isEqualToNumber:customer[ID]]) {
            customerIdx = idx;
            *stop = YES;
        }
    }];

    if (customerIdx != NSNotFound) {
        self.customers[customerIdx] = customer;
    }
    
    [self updateSearchResultsForSearchString:self.searchDisplayController.searchBar.text];
    [self.searchDisplayController.searchResultsTableView reloadData];
    [self.tableView reloadData];
}

@end
