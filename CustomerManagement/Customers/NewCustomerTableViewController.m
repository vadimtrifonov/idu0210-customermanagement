#import "NewCustomerTableViewController.h"
#import "Customers.h"
#import "NSDate+CustomerBirthDate.h"

@interface NewCustomerTableViewController () <UITextFieldDelegate>
@property(weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property(weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property(weak, nonatomic) IBOutlet UITextField *identityCodeTextField;
@property(weak, nonatomic) IBOutlet UITextField *noteTextField;
@property(weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property(strong, nonatomic) IBOutlet UIDatePicker *birthdayDatePicker;
@end

@implementation NewCustomerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.birthdayTextField.inputView = self.birthdayDatePicker;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Actions

- (IBAction)cancel:(id)sender {
    [self.delegate newCustomerTableViewControllerDidCancel];
}

- (IBAction)done:(id)sender {
    NSDictionary *customer = @{
            FIRST_NAME : self.firstNameTextField.text,
            LAST_NAME : self.lastNameTextField.text,
            IDENTITY_CODE : self.identityCodeTextField.text,
            NOTE : self.noteTextField.text,
            BIRTH_DATE : [NSDate iso8601FormattedFromFormattedDate:self.birthdayTextField.text]
    };
    [self.delegate newCustomerTableViewControllerDidGoBackWithNewCustomer:customer];
}

- (IBAction)birthdayDateChanged {
    self.birthdayTextField.text = self.birthdayDatePicker.date.formattedDate;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
