#import <UIKit/UIKit.h>

@protocol NewCustomerTableViewControllerDelegate

- (void)newCustomerTableViewControllerDidCancel;
- (void)newCustomerTableViewControllerDidGoBackWithNewCustomer:(NSDictionary *)customer;

@end

@interface NewCustomerTableViewController : UITableViewController

@property (weak, nonatomic) id <NewCustomerTableViewControllerDelegate> delegate;

@end
