#import <UIKit/UIKit.h>

@protocol CustomerTableViewControllerDelegate

- (void)customerTableViewControllerDidUpdateCustomer:(NSDictionary *)customer;

@end

@interface CustomerTableViewController : UITableViewController

@property (weak, nonatomic) id <CustomerTableViewControllerDelegate> delegate;
@property (copy, nonatomic) NSDictionary *customer;

@end
