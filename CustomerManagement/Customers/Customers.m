#import "Customers.h"
#import "CustomerManagementClient.h"

@implementation Customers

+ (void)loadCustomersWithBlock:(void (^)(NSArray *addresses, NSError *error))block {
    [[CustomerManagementClient sharedClient] GET:@"/customers" parameters:nil success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

+ (void)createCustomer:(NSDictionary *)customer withBlock:(void (^)(NSDictionary *customer, NSError *error))block {
    [[CustomerManagementClient sharedClient] POST:@"/customers" parameters:customer success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

+ (void)updateCustomer:(NSDictionary *)customer withBlock:(void (^)(NSDictionary *customer, NSError *error))block {
    [[CustomerManagementClient sharedClient] POST:[customer[ID] stringValue] parameters:customer success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

+ (void)deleteCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSError *error))block {
    [[CustomerManagementClient sharedClient] DELETE:[customerId stringValue] parameters:nil success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(error);
        }
    }];
}

@end
