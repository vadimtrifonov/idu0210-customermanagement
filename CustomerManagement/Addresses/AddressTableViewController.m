#import "AddressTableViewController.h"
#import "Addresses.h"
#import "SVProgressHUD.h"

@interface AddressTableViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *postIndexTextField;
@property (weak, nonatomic) IBOutlet UITextField *houseTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *countyTextField;
@end

@implementation AddressTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;

    self.addressTextField.text = self.address[ADDRESS];
    self.houseTextField.text = self.address[HOUSE];
    self.cityTextField.text = self.address[CITY];
    self.countyTextField.text = self.address[COUNTY];
    self.postIndexTextField.text = self.address[POST_INDEX];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    if (editing) {
        return;
    }

    [self updateAddress];
    [self.view endEditing:YES];
}

- (void)updateAddress {
    NSMutableDictionary *updatedAddress = [self.address mutableCopy];
    updatedAddress[ADDRESS] = self.addressTextField.text;
    updatedAddress[HOUSE] = self.houseTextField.text;
    updatedAddress[CITY] = self.cityTextField.text;
    updatedAddress[COUNTY] = self.countyTextField.text;
    updatedAddress[POST_INDEX] = self.postIndexTextField.text;

    if (![self.address isEqualToDictionary:updatedAddress]) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [Addresses updateAddress:updatedAddress forCustomerWithId:self.customerId withBlock:^(NSDictionary *address, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:@"Failed"];
            }
            else {
                self.address = address;
                [self.delegate addressTableViewControllerDidUpdateAddress:address];
                [SVProgressHUD showSuccessWithStatus:@"Updated"];
            }
        }];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return self.tableView.editing;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITableViewDataSource

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

@end
