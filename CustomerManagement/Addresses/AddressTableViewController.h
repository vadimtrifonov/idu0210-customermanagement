#import <UIKit/UIKit.h>

@protocol CustomerTableViewControllerDelegate;

@protocol AddressTableViewControllerDelegate

- (void)addressTableViewControllerDidUpdateAddress:(NSDictionary *)address;

@end

@interface AddressTableViewController : UITableViewController

@property (weak, nonatomic) id <AddressTableViewControllerDelegate> delegate;
@property (strong, nonatomic) NSNumber *customerId;
@property (copy, nonatomic) NSDictionary *address;

@end
