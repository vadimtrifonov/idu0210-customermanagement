#import <Foundation/Foundation.h>

#define ID @"id"
#define POST_INDEX @"postIndex"
#define HOUSE @"house"
#define ADDRESS @"address"
#define CITY @"city"
#define COUNTY @"county"

@interface Addresses : NSObject

+ (void)loadAddressesForCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSArray *addresses, NSError *error))block;
+ (void)createAddress:(NSDictionary *)address forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSDictionary *address, NSError *error))block;
+ (void)updateAddress:(NSDictionary *)address forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSDictionary *address, NSError *error))block;
+ (void)deleteAddressWithId:(NSNumber *)addressId forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSError *error))block;

@end