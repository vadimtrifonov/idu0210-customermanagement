#import "Addresses.h"
#import "CustomerManagementClient.h"

@implementation Addresses

+ (void)loadAddressesForCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSArray *addresses, NSError *error))block {
    NSString *path = [[customerId stringValue] stringByAppendingString:@"/addresses"];

    [[CustomerManagementClient sharedClient] GET:path parameters:nil success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

+ (void)createAddress:(NSDictionary *)address forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSDictionary *address, NSError *error))block {
    NSString *path = [[customerId stringValue] stringByAppendingString:@"/addresses"];

    [[CustomerManagementClient sharedClient] POST:path parameters:address success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

+ (void)updateAddress:(NSDictionary *)address forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSDictionary *address, NSError *error))block {
    NSString *path = [NSString stringWithFormat:@"%@/addresses/%@", [customerId stringValue], [address[ID] stringValue]];

    [[CustomerManagementClient sharedClient] POST:path parameters:address success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

+ (void)deleteAddressWithId:(NSNumber *)addressId forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSError *error))block {
    NSString *path = [NSString stringWithFormat:@"%@/addresses/%@", [customerId stringValue], [addressId stringValue]];

    [[CustomerManagementClient sharedClient] DELETE:path parameters:nil success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(error);
        }
    }];
}

@end
