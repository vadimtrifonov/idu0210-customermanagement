#import "NewAddressTableViewController.h"
#import "Addresses.h"

@interface NewAddressTableViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *postIndexTextField;
@property (weak, nonatomic) IBOutlet UITextField *houseTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *countyTextField;
@end

@implementation NewAddressTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Actions

- (IBAction)cancel:(id)sender {
    [self.delegate newAddressTableViewControllerDidCancel];
}

- (IBAction)done:(id)sender {
    NSDictionary *address = @{
            POST_INDEX:self.postIndexTextField.text,
            HOUSE:self.houseTextField.text,
            ADDRESS:self.addressTextField.text,
            CITY:self.cityTextField.text,
            COUNTY:self.countyTextField.text
    };
    [self.delegate newAddressTableViewControllerDidGoBackWithNewAddress:address];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
