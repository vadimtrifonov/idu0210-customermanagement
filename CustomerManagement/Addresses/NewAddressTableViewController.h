#import <UIKit/UIKit.h>

@protocol NewAddressTableViewControllerDelegate

- (void)newAddressTableViewControllerDidCancel;
- (void)newAddressTableViewControllerDidGoBackWithNewAddress:(NSDictionary *)address;

@end

@interface NewAddressTableViewController : UITableViewController

@property (weak, nonatomic) id <NewAddressTableViewControllerDelegate> delegate;

@end
