#import "AddressesTabelViewController.h"
#import "Addresses.h"
#import "AddressTableViewController.h"
#import "NewAddressTableViewController.h"
#import "SVProgressHUD.h"

@interface AddressesTabelViewController () <NewAddressTableViewControllerDelegate, AddressTableViewControllerDelegate>
@property (strong, nonatomic) NSMutableArray *addresses;
@end

@implementation AddressesTabelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.refreshControl addTarget:self
                            action:@selector(refresh:)
                  forControlEvents:UIControlEventValueChanged];

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self loadAddresses];
}

- (void)loadAddresses {
    [Addresses loadAddressesForCustomerWithId:self.customerId withBlock:^(NSArray *addresses, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:@"Failed"];
        }
        else {
            self.addresses = [addresses mutableCopy];
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        }

        [self.refreshControl endRefreshing];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showAddress"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *address = self.addresses[indexPath.row];

        [segue.destinationViewController setDelegate:self];
        [segue.destinationViewController setCustomerId:self.customerId];
        [segue.destinationViewController setAddress:address];
    }
    if ([segue.identifier isEqualToString:@"newAddress"]) {
        UINavigationController *nc = segue.destinationViewController;
        [(NewAddressTableViewController *) nc.topViewController setDelegate:self];
    }
}

#pragma mark - Actions

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self loadAddresses];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.addresses.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Address Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    NSDictionary *address = self.addresses[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", address[ADDRESS], address[HOUSE]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@", address[CITY], address[COUNTY]];

    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [Addresses deleteAddressWithId:self.addresses[indexPath.row][ID] forCustomerWithId:self.customerId withBlock:^(NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:@"Failed"];
            }
            else {
                [self.addresses removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [SVProgressHUD showSuccessWithStatus:@"Deleted"];
            }
        }];
    }
}

#pragma mark - NewAddressTableViewControllerDelegate

- (void)newAddressTableViewControllerDidCancel {
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)newAddressTableViewControllerDidGoBackWithNewAddress:(NSDictionary *)address {
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [Addresses createAddress:address forCustomerWithId:self.customerId withBlock:^(NSDictionary *address,  NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:@"Failed"];
        }
        else {
            [self.addresses addObject:address];
            [self.tableView reloadData];
            [SVProgressHUD showSuccessWithStatus:@"Created"];
        }
    }];
}

#pragma mark - AddressTableViewControllerDelegate

- (void)addressTableViewControllerDidUpdateAddress:(NSDictionary *)address {
    __block NSUInteger addressIdx = NSNotFound;
    [self.addresses enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj[ID] isEqualToNumber:address[ID]]) {
            addressIdx = idx;
            *stop = YES;
        }
    }];

    if (addressIdx != NSNotFound) {
        self.addresses[addressIdx] = address;
    }

    [self.tableView reloadData];
}

@end
