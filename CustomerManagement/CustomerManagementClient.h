#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

#define CUSTOMER_MANAGEMENT_CLIENT_URL @"http://localhost:8401/customers/"

@interface CustomerManagementClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end