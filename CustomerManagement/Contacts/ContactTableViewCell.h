#import <UIKit/UIKit.h>

@interface ContactTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *contactType;
@property (weak, nonatomic) IBOutlet UITextField *contactValue;

@end
