#import <UIKit/UIKit.h>

@protocol ContactTypesTableViewControllerDelegate

- (void)contactTypeTableViewControllerDidCancel;
- (void)contactTypeTableViewControllerDidGoBackWithContactType:(NSDictionary *)contactType;

@end


@interface ContactTypesTableViewController : UITableViewController

@property (copy, nonatomic) NSArray *contactTypes;
@property (strong, nonatomic) NSNumber *selectedContactType;
@property (weak, nonatomic) id <ContactTypesTableViewControllerDelegate> delegate;

@end
