#import <SVProgressHUD/SVProgressHUD.h>
#import "ContactsTableViewController.h"
#import "ContactTableViewCell.h"
#import "ContactTypesTableViewController.h"
#import "Contacts.h"

@interface ContactsTableViewController () <UITextFieldDelegate, ContactTypesTableViewControllerDelegate>
@property (copy, nonatomic) NSArray *uneditedContacts;
@property (strong, nonatomic) NSMutableArray *contacts;
@property (strong, nonatomic) NSArray *contactTypes;
@property (strong, nonatomic) NSIndexPath *indexPathOfLastSegue;
@end

@implementation ContactsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self enableRefreshControl:YES];

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self loadContactsWithBlock:nil];
}

- (void)loadContactsWithBlock:(void (^)(void))block {
    [Contacts loadContactTypesWithBlock:^(NSArray *contactTypes, NSError *error) {
        self.contactTypes = contactTypes;

        [Contacts loadContactsForCustomerWithId:self.customerId withBlock:^(NSArray *contacts, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:@"Failed"];
            }
            else {
                self.uneditedContacts = contacts;
                self.contacts = [self mutableContactsCopy:contacts];
                [self.tableView reloadData];
                block ? block() : [SVProgressHUD dismiss];
            }

            [self.refreshControl endRefreshing];
        }];
    }];
}

- (NSMutableArray *)mutableContactsCopy:(NSArray *)contacts {
    NSMutableArray *copy = [NSMutableArray array];
    for (NSDictionary *contact in contacts) {
        [copy addObject:[contact mutableCopy]];
    }
    return copy;
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return self.editing;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.indexPathOfLastSegue = [self.tableView indexPathForSelectedRow];
    
    UINavigationController *nc = segue.destinationViewController;
    ContactTypesTableViewController *cttvc = (ContactTypesTableViewController *) nc.topViewController;
    cttvc.contactTypes = self.contactTypes;
    cttvc.selectedContactType = self.contacts[self.indexPathOfLastSegue.row][TYPE];
    cttvc.delegate = self;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self enableRefreshControl:!editing];

    if (editing) {
        if (!self.contacts.count) {
            self.contacts = [NSMutableArray arrayWithObject:[@{TYPE:@(1), VALUE:@"", ORDER:@(0)} mutableCopy]];
            [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        return;
    }

    [self.view endEditing:YES];
    [self updateContacts];
}

- (void)enableRefreshControl:(BOOL)enable {
    if (enable) {
        self.refreshControl = [[UIRefreshControl alloc] init];
        [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    }
    else {
        self.refreshControl = nil;
    }
}

- (void)updateContacts {
    if (!self.contacts.count || [self.contacts isEqualToArray:self.uneditedContacts]) {
        return;
    }

    [self.contacts enumerateObjectsUsingBlock:^(id contact, NSUInteger idx, BOOL *stop) {
        contact[ORDER] = @(idx);
    }];

    NSMutableArray *updatedContacts = [NSMutableArray array];
    NSMutableArray *createdContacts = [NSMutableArray array];
    for (NSDictionary *contact in self.contacts) {
        NSUInteger index = [self.uneditedContacts indexOfObjectPassingTest:^BOOL(id uneditedContact, NSUInteger idx, BOOL *stop) {
            return [contact[ID] isEqual:uneditedContact[ID]];
        }];

        if (index == NSNotFound) {
            [createdContacts addObject:contact];
        }
        else if (![contact isEqualToDictionary:self.uneditedContacts[index]]) {
            [updatedContacts addObject:contact];
        }
    }

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [Contacts createBatchOfContacts:createdContacts forCustomerWithId:self.customerId withBlock:^(NSError *error) {
        if (error) {
            [self loadContactsWithBlock:^{
                [SVProgressHUD showErrorWithStatus:@"Failed"];
            }];
            return;
        }

        [Contacts updateBatchOfContacts:updatedContacts forCustomerWithId:self.customerId withBlock:^(NSError *error) {
            [self loadContactsWithBlock:^{
                error ? [SVProgressHUD showErrorWithStatus:@"Failed"] : [SVProgressHUD showSuccessWithStatus:@"Updated"];
            }];
        }];
    }];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (NSArray *)uneditedContacts {
    return _uneditedContacts ?: [NSArray array];
}

- (NSArray *)contactTypes {
    return _contactTypes ?: [NSArray array];
}

#pragma mark - Actions

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self loadContactsWithBlock:nil];
}

#pragma mark - ContactTypesTableViewControllerDelegate

- (void)contactTypeTableViewControllerDidCancel {
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)contactTypeTableViewControllerDidGoBackWithContactType:(NSDictionary *)contactType {
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    ContactTableViewCell *cell = (ContactTableViewCell *)[self.tableView cellForRowAtIndexPath:self.indexPathOfLastSegue];

    self.contacts[self.indexPathOfLastSegue.row][TYPE] = contactType[ID];
    cell.contactType.text = contactType[NAME];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return self.tableView.editing;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    id textFieldSuperview = textField;
    while (![textFieldSuperview isKindOfClass:[ContactTableViewCell class]]) {
        textFieldSuperview = [textFieldSuperview superview];
    }
    NSIndexPath *indexPath = [self.tableView indexPathForCell:(ContactTableViewCell *)textFieldSuperview];

    self.contacts[indexPath.row][VALUE] = textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Contact Cell";
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    NSUInteger idx = [self.contactTypes indexOfObjectPassingTest:^BOOL(id contactType, NSUInteger idx, BOOL *stop) {
        return [self.contacts[indexPath.row][TYPE] isEqual:contactType[ID]];
    }];

    cell.contactType.text = (idx != NSNotFound) ? self.contactTypes[idx][NAME] : nil;
    cell.contactValue.text = self.contacts[indexPath.row][VALUE];
    cell.contactValue.delegate = self;

    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.editing ? UITableViewCellEditingStyleInsert : UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleInsert) {
        NSMutableDictionary *contact = [self.contacts[indexPath.row] mutableCopy];
        [contact removeObjectForKey:ID];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];

        [self.contacts insertObject:contact atIndex:newIndexPath.row];
        [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if (editingStyle == UITableViewCellEditingStyleDelete) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [Contacts deleteContactWithId:self.contacts[indexPath.row][ID] forCustomerWithId:self.customerId withBlock:^(NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:@"Failed"];
            }
            else {
                [self.contacts removeObjectAtIndex:indexPath.row];
                self.uneditedContacts = self.contacts;
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [SVProgressHUD showSuccessWithStatus:@"Deleted"];
            }
        }];
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    if (self.contacts.count > 1) {
        NSDictionary *contact = self.contacts[fromIndexPath.row];
        [self.contacts removeObjectAtIndex:fromIndexPath.row];
        [self.contacts insertObject:contact atIndex:toIndexPath.row];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
