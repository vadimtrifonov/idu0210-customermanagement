#import <Foundation/Foundation.h>

#define ID @"id"
#define TYPE @"type"
#define NAME @"name"
#define VALUE @"value"
#define ORDER @"order"

@interface Contacts : NSObject

+ (void)loadContactsForCustomerWithId:(NSNumber *)number withBlock:(void (^)(NSArray *contacts, NSError *error))block;
+ (void)createBatchOfContacts:(NSArray *)contacts forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSError *error))block;
+ (void)updateBatchOfContacts:(NSArray *)contacts forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSError *error))block;
+ (void)deleteContactWithId:(NSNumber *)contactId forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSError *error))block;
+ (void)loadContactTypesWithBlock:(void (^)(NSArray *contactTypes, NSError *error))block;

@end