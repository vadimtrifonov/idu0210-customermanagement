#import "ContactTypesTableViewController.h"
#import "Contacts.h"

@implementation ContactTypesTableViewController {
    NSUInteger _checkmarkedRow;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _checkmarkedRow = [self.contactTypes indexOfObjectPassingTest:^BOOL(id contactType, NSUInteger idx, BOOL *stop) {
        return [self.selectedContactType isEqual:contactType[ID]];
    }];
}

- (NSArray *)contactTypes {
    return _contactTypes ?: [NSArray array];
}

#pragma mark - IBActions

- (IBAction)cancel:(id)sender {
    [self.delegate contactTypeTableViewControllerDidCancel];
}

- (IBAction)done:(id)sender {
    NSDictionary *contactType = (_checkmarkedRow != NSNotFound) ? self.contactTypes[_checkmarkedRow] : nil;
    [self.delegate contactTypeTableViewControllerDidGoBackWithContactType:contactType];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contactTypes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Contact Type Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = self.contactTypes[indexPath.row][NAME];   
    if (indexPath.row == _checkmarkedRow) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == _checkmarkedRow) {
        return;
    }

    NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:_checkmarkedRow inSection:0];
    [self.tableView cellForRowAtIndexPath:oldIndexPath].accessoryType = UITableViewCellAccessoryNone;
    [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;

    _checkmarkedRow = indexPath.row;
}

@end
