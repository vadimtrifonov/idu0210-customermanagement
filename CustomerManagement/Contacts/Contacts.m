#import "Contacts.h"
#import "CustomerManagementClient.h"
#import "AFURLConnectionOperation.h"
#import "AFHTTPRequestOperation.h"


@implementation Contacts

+ (void)loadContactsForCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSArray *contacts, NSError *error))block {
    NSString *path = [[customerId stringValue] stringByAppendingString:@"/contacts"];

    [[CustomerManagementClient sharedClient] GET:path parameters:nil success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

+ (void)createBatchOfContacts:(NSArray *)contacts
            forCustomerWithId:(NSNumber *)customerId
                    withBlock:(void (^)(NSError *error))block
{
    if (!contacts.count) {
        block(nil);
    }
    
    NSArray *operations = [self operationsForContacts:contacts pathBlock:^(NSString *contactId) {
        return [NSString stringWithFormat:@"%@%@/contacts", CUSTOMER_MANAGEMENT_CLIENT_URL, [customerId stringValue]];
    }];

    NSArray *batchedOperations = [AFURLConnectionOperation batchOfRequestOperations:operations progressBlock:nil completionBlock:^(NSArray *operations) {
        [self processOperations:operations withBlock:block];
    }];

    [[NSOperationQueue mainQueue] addOperations:batchedOperations waitUntilFinished:NO];
}

+ (void)updateBatchOfContacts:(NSArray *)contacts
            forCustomerWithId:(NSNumber *)customerId
                    withBlock:(void (^)(NSError *error))block
{
    if (!contacts.count) {
        block(nil);
    }
    
    NSArray *operations = [self operationsForContacts:contacts pathBlock:^(NSString *contactId) {
        return [NSString stringWithFormat:@"%@%@/contacts/%@", CUSTOMER_MANAGEMENT_CLIENT_URL, [customerId stringValue], contactId];
    }];

    NSArray *batchedOperations = [AFURLConnectionOperation batchOfRequestOperations:operations progressBlock:nil completionBlock:^(NSArray *operations) {
        [self processOperations:operations withBlock:block];
    }];

    [[NSOperationQueue mainQueue] addOperations:batchedOperations waitUntilFinished:NO];
}

+ (void)deleteContactWithId:(NSNumber *)contactId forCustomerWithId:(NSNumber *)customerId withBlock:(void (^)(NSError *error))block {
    NSString *path = [NSString stringWithFormat:@"%@/contacts/%@", [customerId stringValue], [contactId stringValue]];

    [[CustomerManagementClient sharedClient] DELETE:path parameters:nil success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(error);
        }
    }];
}

+ (void)loadContactTypesWithBlock:(void (^)(NSArray *contactTypes, NSError *error))block {
    NSString *path = @"contact-types";

    [[CustomerManagementClient sharedClient] GET:path parameters:nil success:^(NSURLSessionDataTask *operation, id JSON) {
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

+ (NSArray *)operationsForContacts:(NSArray *)contacts pathBlock:(NSString * (^)(NSString *contactId))pathBlock {
    NSMutableArray *operations = [NSMutableArray array];

    for (NSDictionary *contact in contacts) {
        NSString *path = pathBlock ? pathBlock([contact[ID] stringValue]) : nil;
        NSURLRequest *request = [((AFJSONRequestSerializer *) [CustomerManagementClient sharedClient].requestSerializer) requestWithMethod:@"POST" URLString:path parameters:contact];
        [operations addObject:[[AFHTTPRequestOperation alloc] initWithRequest:request]];
    }

    return operations;
}

+ (void)processOperations:(NSArray *)operations withBlock:(void (^)(NSError *error))block {
    if (!block) {
        return;
    }

    for (AFHTTPRequestOperation *operation in operations) {
        if (operation.error) {
            block(operation.error);
            return;
        }
    }
    block(nil);
}

@end