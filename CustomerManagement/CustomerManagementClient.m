#import "CustomerManagementClient.h"
#import "AFHTTPSessionManager.h"

@implementation CustomerManagementClient

+ (CustomerManagementClient *)sharedClient {
    static CustomerManagementClient *sharedClient;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[CustomerManagementClient alloc] initWithBaseURL:[NSURL URLWithString:CUSTOMER_MANAGEMENT_CLIENT_URL]];
        sharedClient.requestSerializer = [AFJSONRequestSerializer serializer];
        sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
    });

    return sharedClient;
}

@end